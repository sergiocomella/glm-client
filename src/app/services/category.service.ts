import { Category } from '../models/category.model';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

import {environment} from '../../environments/environment';

const BACKEND_URL = environment.apiURL + '/categories';

/**
 * Se lo dichiaro qua e non in providers di app.module.ts
 * allora creo una singola istanza per tutta l'app.
 */
@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  private categories: Category[] = [];
  private categoriesUpdated = new Subject<{ categories: Category[] }>();

  constructor(private http: HttpClient, private router: Router) {}

  getAllCategories() {
    this.http
      .get<{categories: any}>(
        BACKEND_URL + '/all'
      )
      .pipe(
        map(categoryData => {
          return {
            categories: categoryData.categories.map(category => {
              return {
                id: category._id,
                categoryName: category.categoryName
              };
            })
          };
        })
      )
      .subscribe(transformedCategoryData => {
        this.categories = transformedCategoryData.categories;
        this.categoriesUpdated.next({
          categories: [...this.categories],
        });
      });
  }

  getCategoryUpdateListener() {
    return this.categoriesUpdated.asObservable();
  }

  getCategoryForOptions(){
    return new Promise((resolve, rejects) => {
      this.http
        .get<{categories: Category[]}>(
          BACKEND_URL + '/all'
        )
        .toPromise()
        .then(result => {
          resolve(result.categories);
        }, err => {
          rejects(err);
        })
        .catch(err => {
          alert('Impossibile recuperare le categorie');
          console.log(err)
        });
    });
  }

  addCategory(categoryName: string) {
    this.http
      .post<{ message: string; post: Category }>(
        BACKEND_URL + '/add',
        {"categoryName": categoryName}
      )
      .subscribe(response => {
        this.getAllCategories();
        //this.router.navigate(["/categories"]);
      });
  }

  deleteCategory(categoryId: string){
    return this.http.delete(BACKEND_URL + '/' + categoryId);
  }

}
