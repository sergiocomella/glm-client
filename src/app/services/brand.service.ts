import { Brand } from '../models/brand.model';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

import {environment} from '../../environments/environment';
import {Typology} from '../models/typology.model';

const BACKEND_URL = environment.apiURL + '/brands';

/**
 * Se lo dichiaro qua e non in providers di app.module.ts
 * allora creo una singola istanza per tutta l'app.
 */
@Injectable({
  providedIn: 'root'
})
export class BrandService {
  private _brands: Brand[] = [];
  private _brandsUpdated = new Subject<{ brands: Brand[] }>();

  constructor(private http: HttpClient, private router: Router) {}

  getAllBrands() {
    this.http
      .get<{brands: any}>(
        BACKEND_URL + '/all'
      )
      .pipe(
        map(brandData => {
          return {
            brands: brandData.brands.map(brand => {
              return {
                id: brand._id,
                brandName: brand.brandName
              };
            })
          };
        })
      )
      .subscribe(transformedBrandData => {
        this._brands = transformedBrandData.brands;
        this._brandsUpdated.next({
          brands: [...this._brands],
        });
      });
  }

  getBrandUpdateListener() {
    return this._brandsUpdated.asObservable();
  }

  getBrandForOptions(){
    return new Promise((resolve, rejects) => {
      this.http
        .get<{brands: Brand[]}>(
          BACKEND_URL + '/all'
        )
        .toPromise()
        .then(result => {
          resolve(result.brands);
        }, err => {
          rejects(err);
        })
        .catch(err => {
          alert('Impossibile recuperare le tipologie');
          console.log(err)
        });
    });
  }

  addBrand(brandName: string) {
    this.http
      .post<{ message: string; post: Brand }>(
        BACKEND_URL + '/add',
        {"brandName": brandName}
      )
      .subscribe(response => {
        this.getAllBrands();
        //this.router.navigate(["/brands"]);
      });
  }

  deleteBrand(brandId: string){
    return this.http.delete(BACKEND_URL + '/' + brandId);
  }
}
