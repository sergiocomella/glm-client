import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {map} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {List} from '../models/list.model';
import {saveAs} from 'file-saver';
import {Row} from '../models/row.model';

const BACKEND_URL = environment.apiURL + '/lists';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  private _lists: List[] = [];
  private _listsUpdated = new Subject<{ lists: List[] }>();
  private _newListId: string;

  private _rows: Row[] = [];
  private _rowsUpdated = new Subject<{ rows: Row[] }>();

  private _editListTile: string;

  constructor(private http: HttpClient) {
  }

  private _convertDate(date) {
    let d = new Date(date);
    return d.getDate() + '/' + d.getMonth() + '/' + d.getFullYear()
      + ' ' + d.getHours() + ':' + d.getMinutes();
  }

  getAllLists() {
    this.http
      .get<{ lists: any }>(
        BACKEND_URL + '/all'
      )
      .pipe(
        map(listData => {
          return {
            lists: listData.lists.map(list => {
              return {
                id: list._id,
                listTitle: list.listTitle,
                createdAt: this._convertDate(list.createdAt),
                updatedAt: this._convertDate(list.updatedAt)
              };
            })
          };
        })
      )
      .subscribe(transformedListData => {
        this._lists = transformedListData.lists;
        this._listsUpdated.next({
          lists: [...this._lists],
        });
      });
  }

  getListsUpdateListener() {
    return this._listsUpdated.asObservable();
  }

  generateList(listTitle: string) {
    return new Promise((resolve, rejects) => {
      this.http
        .post(
          BACKEND_URL + '/addList', {'listTitle': listTitle}
        ).toPromise()
        .then(result => {
          this._newListId = result['id'];
          resolve(result);
        }, err => {
          rejects(err);
        })
        .catch(err => alert('Qualcosa è andato storto (Verificare l\'univocità del nome)'));
    });
  }

  getList( id: string){
    return new Promise((resolve, rejects) => {
      this.http
        .get<{listTitle: string}>(BACKEND_URL + '/list/' + id)
        .toPromise()
        .then(list => {
          resolve(list.listTitle);
        }, err => {
          rejects(err);
        })
        .catch(err => {
          alert('Impossibile recuperare il titolo della lista');
          console.log(err)
        });
    });
  }

  getRowsList( idList: string){
    this.http
      .get<{listTitle: string, rows: any}>(
        BACKEND_URL + '/list/' + idList
      )
      .pipe(
        map( list => {
          return {
            listTitle: list.listTitle,
            rows: list.rows.map(row => {
              return {
                id: row._id,
                brand: row.brand,
                category: row.category,
                typology: row.typology,
                quantity: row.quantity,
                price: row.price,
                code: row.code,
                gender: row.gender,
                season: row.season,
                case: row.case
              };
            })
          };
        })
      )
      .subscribe(list => {
        this._editListTile = list.listTitle;
        this._rows = list.rows;
        this._rowsUpdated.next({
          rows: [...this._rows],
        });
      });
  }

  getRowsUpdateListener() {
    return this._rowsUpdated.asObservable();
  }

  getEditListTitle(){
    return this._editListTile;
  }

  getRowById(rowId: string){
    return new Promise((resolve, rejects) => {
      this.http
        .get<{row: any}>(BACKEND_URL + '/getRow/' + rowId)
        .toPromise()
        .then(result => {
          resolve(result.row);
        }, err => {
          rejects(err);
        })
        .catch(err => {
          alert('Impossibile modificare la riga');
          console.log(err)
        });
    });
  }

  updateRow(listId: string, updatedRow: any) {
    this.http
      .put(
        BACKEND_URL + '/updateRow',
        { listId: listId , row: updatedRow}
      )
      .subscribe(response => {
        this.getRowsList(listId);
      });

  }

  addRowInList(listId: string, row: Row){
    this.http
      .put(
        BACKEND_URL + '/addRow',
        { listId: listId , row: row}
      )
      .subscribe(response => {
        this.getRowsList(listId);
      });
  }

  deleteRowFromList(listId: string, rowId: string) {
    this.http
      .delete(
        BACKEND_URL + '/deleteRow/' + listId + '/' + rowId
      )
      .subscribe(response => {
        console.log(response);
        this.getRowsList(listId);
      });
  }

  deleteList(listId: string) {
    this.http
      .delete(
        BACKEND_URL + '/deleteList/' + listId
      )
      .subscribe(response => {
        console.log(response);
        this.getAllLists();
      });
  }

  downloadList(listId: string, listTitle: string) {
    this.http
      .post(
        BACKEND_URL + '/download', {listId: listId}, {responseType: "blob"})
      .subscribe(
        result => {
          saveAs(result, listTitle + '.xlsx')
        },
        error => console.log("Errore nel download: " + error.message))
  }
}
