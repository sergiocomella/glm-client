import { TestBed } from '@angular/core/testing';

import { TypologyService } from './typology.service';

describe('TypologyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypologyService = TestBed.get(TypologyService);
    expect(service).toBeTruthy();
  });
});
