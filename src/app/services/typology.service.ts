import { Typology } from '../models/Typology.model';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

import {environment} from '../../environments/environment';
import {Category} from '../models/category.model';

const BACKEND_URL = environment.apiURL + '/typologies';

/**
 * Se lo dichiaro qua e non in providers di app.module.ts
 * allora creo una singola istanza per tutta l'app.
 */
@Injectable({
  providedIn: 'root'
})
export class TypologyService {
  private _typologies: Typology[] = [];
  private _typologiesUpdated = new Subject<{ typologies: Typology[] }>();

  constructor(private http: HttpClient, private router: Router) {}

  getAllTypologies() {
    this.http
      .get<{typologies: any}>(
        BACKEND_URL + '/all'
      )
      .pipe(
        map(typologyData => {
          return {
            typologies: typologyData.typologies.map(typology => {
              return {
                id: typology._id,
                typologyName: typology.typologyName
              };
            })
          };
        })
      )
      .subscribe(transformedTypologyData => {
        this._typologies = transformedTypologyData.typologies;
        this._typologiesUpdated.next({
          typologies: [...this._typologies],
        });
      });
  }

  getTypologyUpdateListener() {
    return this._typologiesUpdated.asObservable();
  }

  getTypologyForOptions(){
    return new Promise((resolve, rejects) => {
      this.http
        .get<{typologies: Typology[]}>(
          BACKEND_URL + '/all'
        )
        .toPromise()
        .then(result => {
          resolve(result.typologies);
        }, err => {
          rejects(err);
        })
        .catch(err => {
          alert('Impossibile recuperare le tipologie');
          console.log(err)
        });
    });
  }

  addTypology(typologyName: string) {
    this.http
      .post<{ message: string; typology: Typology }>(
        BACKEND_URL + '/add',
        {"typologyName": typologyName}
      )
      .subscribe(response => {
        this.getAllTypologies();
        //this.router.navigate(["/brands"]);
      });
  }

  deleteTypology(typologyId: string){
    return this.http.delete(BACKEND_URL + '/' + typologyId);
  }
}
