import {Row} from './row.model';

export interface List {
  id: string,
  title: string,
  rows: Row[],
  createdAt: Date,
  updatedAt: Date
}
