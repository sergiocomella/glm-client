export interface Row {
  id: string,
  brand: string,
  category: string,
  typology: string,
  quantity: number,
  price: number,
  code: string,
  gender: string,
  season: string,
  case: number
}
