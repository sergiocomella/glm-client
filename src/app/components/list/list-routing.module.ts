import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import {AuthGuard} from '../auth/auth.guard';
import {ListsTableComponent} from './lists-table/lists-table.component';
import {ListFormComponent} from './list-form/list-form.component';

const routers: Routes = [
  { path: "lists", component: ListsTableComponent, canActivate: [AuthGuard]},
  { path: "list-form", component: ListFormComponent, canActivate: [AuthGuard]},
  { path: "list-form/:listId", component: ListFormComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routers)],
  exports: [RouterModule]
})
export class ListRoutingModule {}
