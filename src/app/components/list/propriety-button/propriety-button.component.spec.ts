import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProprietyButtonComponent } from './propriety-button.component';

describe('ProprietyButtonComponent', () => {
  let component: ProprietyButtonComponent;
  let fixture: ComponentFixture<ProprietyButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProprietyButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProprietyButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
