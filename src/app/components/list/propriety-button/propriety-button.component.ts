import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IToggleProduct} from '../../../interfaces/IToggleProduct.interface';

@Component({
  selector: 'app-propriety-button',
  templateUrl: './propriety-button.component.html',
  styleUrls: ['./propriety-button.component.css']
})
export class ProprietyButtonComponent implements OnInit {

  @Input()
  public list: any;

  @Input()
  public type: any;

  @Output()
  public toggleProduct: EventEmitter<IToggleProduct> = new EventEmitter();

  propName: string;
  constructor() { }

  ngOnInit(): void {
    this.propName = this.type+'Name';
  }

  public handleClick(element: string, valueSelected: string): void{
    /**
     * Propago un evento all'esterno di questo componente
     */
    this.toggleProduct.emit({
      element: element,
      valueSelected: valueSelected
    });

  }

  public sort() {
    this.list.sort((a, b) => a[this.propName].localeCompare(b[this.propName]));
  }

}
