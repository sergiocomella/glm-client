import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AngularMaterialModule } from "../../angular-material.module";
import {ListFormComponent} from './list-form/list-form.component';
import {ListsTableComponent} from './lists-table/lists-table.component';
import {ListRoutingModule} from './list-routing.module';
import { ProprietyButtonComponent } from './propriety-button/propriety-button.component';
import {MatDividerModule} from '@angular/material/divider';
import {SortListByName} from '../../pipes/SortListByName.pipe';
import { NumberButtonComponent } from './number-button/number-button.component';

@NgModule({
  declarations: [
    ListFormComponent,
    ListsTableComponent,
    ProprietyButtonComponent,
    SortListByName,
    NumberButtonComponent
  ],
  exports: [
    ListFormComponent,
    ListsTableComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ListRoutingModule,
    MatDividerModule
  ]
})
export class ListModule {}
