import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ListService} from '../../../services/list.service';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Row} from '../../../models/row.model';
import {List} from '../../../models/list.model';
import {MatTableDataSource} from '@angular/material/table';
import {Subscription} from 'rxjs';
import {MatSort} from '@angular/material/sort';
import {CategoriesService} from '../../../services/category.service';
import {map, startWith} from 'rxjs/operators';
import {TypologyService} from '../../../services/typology.service';
import {BrandService} from '../../../services/brand.service';
import {KeyValue} from '@angular/common';
import {MatPaginator} from '@angular/material/paginator';
import {IToggleProduct} from '../../../interfaces/IToggleProduct.interface';
import {IToggleNumber} from '../../../interfaces/IToggleNumber.interface';

@Component({
  selector: 'app-list-form',
  templateUrl: './list-form.component.html',
  styleUrls: ['./list-form.component.css']
})
export class ListFormComponent implements OnInit {

  private listService: ListService;
  private _listTitle: any;
  private _rowsSub: Subscription;
  private _rows: any;
  private _idRowToUpdate: any =  false;

  formTitle: FormGroup;
  showFormTitle: boolean = true;

  _tempRow;

  utilMapRowProp = {
    brand: 'Marchio',
    typology: 'Tipologia',
    category: 'Categoria',
    quantity: 'Quantità',
    price: 'Prezzo',
    code: 'Codice',
    gender: 'Genere',
    season: 'Stagione',
    case: 'Box'
  }

  list: List = new class implements List {
    createdAt: Date;
    id: string;
    rows: Row[];
    title: string;
    updatedAt: Date;
  };

  //Element passed to child
  brandForChild;
  categoryForChild;
  typologyForChild;
  genderForChild;
  numberForChild;

  @Input() rowPropriety: string;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  displayedColumns: string[] = Object.keys(this.utilMapRowProp);

  formRow: FormGroup;

  autocompleteSelected: {} = {}; //Array of Observable
  protected autocompleteElemets: {} = {}; //Array of string[]

  constructor(listService: ListService,
              private _route: ActivatedRoute,
              public categoryService: CategoriesService,
              public typologyService: TypologyService,
              public brandService: BrandService) {
    this.listService = listService;
  }

  ngOnInit() {

    this.formTitle = new FormGroup({
      listTitle: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(3)]
      })
    });

    this.formRow = new FormGroup({
      brand: new FormControl(null, {}),
      typology: new FormControl(null, {}),
      category: new FormControl(null, {}),
      price: new FormControl(null, {}),
      quantity: new FormControl(null, {}),
      code: new FormControl(null, {}),
      gender: new FormControl(null, {}),
      season: new FormControl(null, {}),
      case: new FormControl(null, {}),
    });
    this.formRow.get('case').setValue(0);
    this._getElementsForAutocomplete();
    //start with brand propriety-list
    this.rowPropriety = 'brand';

    this._tempRow = new class implements Row {
      brand: string;
      typology: string;
      category: string;
      quantity: number;
      price: number;
      code: string;
      season: string;
      case: number;
      gender: string;
      id: string;
    }

    this._route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has("listId")) {
        this.list.id = paramMap.get("listId");
      }
    });

    this._loadRowsList();
    this.displayedColumns.push('options');

  }

  //Clear form and element row
  private _resetTempRow(){
    this._tempRow = new class implements Row {
      brand: string;
      typology: string;
      category: string;
      quantity: number;
      price: number;
      code: string;
      gender: string;
      season: string;
      case: number;
      id: string;
    };
  }

  //For edit list, load all rows of the list selected
  private _loadRowsList(){
    if (!!this.list.id) {
      this.listService.getRowsList(this.list.id);
      this._rowsSub = this.listService
        .getRowsUpdateListener()
        .subscribe((listData: { rows: Row[] }) => {
          this._rows = new MatTableDataSource(listData.rows);
          this._rows.sort = this.sort;
          this._rows.paginator = this.paginator;
        });

      this.list.rows = this._rows;
      this.listService
        .getList(this.list.id)
        .then(title => this.list.title = <string> title);
      this.showFormTitle = false;
    }
  }

  //Create elements for autocomplete
  private _getElementsForAutocomplete() {

    //CATEGORIE
    this.categoryService
      .getCategoryForOptions()
      .then(categories => {
        this.categoryForChild = categories;
        // @ts-ignore
        this.autocompleteElemets['category'] = categories.map(x=>x.categoryName);
        this.autocompleteSelected['category'] = this.formRow.get('category').valueChanges
          .pipe(
            startWith(''),
            map(value => this._filter(value))
          );
      });

    //TIPOLOGIE
    this.typologyService
      .getTypologyForOptions()
      .then(typologies => {
        this.typologyForChild = typologies;
        // @ts-ignore
        this.autocompleteElemets['typology'] = typologies.map(x => x.typologyName);
        this.autocompleteSelected['typology'] = this.formRow.get('typology').valueChanges
          .pipe(
            startWith(''),
            map(value => this._filter(value))
          );
      });

    //MARCHI
    this.brandService
      .getBrandForOptions()
      .then(brands => {
        this.brandForChild = brands;
        // @ts-ignore
        this.autocompleteElemets['brand'] = brands.map(x => x.brandName);
        this.autocompleteSelected['brand'] = this.formRow.get('brand').valueChanges
          .pipe(
            startWith(''),
            map(value => this._filter(value))
          );
      });


    //Pass gender for generate button
    this.genderForChild = [
      {genderName: "UOMO"},
      {genderName: "DONNA"},
      {genderName: "UOMO/DONNA"}
      ];

  }

  //Used for autocomplete
  private _filter(value: string): string[] {
    if(value != null) {
      const filterValue = value.toLowerCase();
      return this.autocompleteElemets[this.rowPropriety].filter(option => option.toLowerCase().includes(filterValue));
    }
  }

  // Preserve original property order
  originalOrder = (a: KeyValue<string,string>, b: KeyValue<string,string>): number => {
    return 0;
  }

  //On save list name, generate list in db
  public generateList(){
    if (this.formTitle.invalid) {
      return;
    }
    this._listTitle = this.formTitle.value.listTitle;
    this.listService
      .generateList(this._listTitle)
      .then(r => {
        this.list.id = r['id'];
        this.list.title = this._listTitle;
        this.showFormTitle = false;

        this.listService.getRowsList(this.list.id);
        this._rowsSub = this.listService
          .getRowsUpdateListener()
          .subscribe((listData: {rows: Row[]}) => {
            this._rows = new MatTableDataSource(listData.rows);
            this._rows.sort = this.sort;
          });

      });
  }


  public getRows(){
    return this._rows;
  }

  //Save or update the row in list

  public addRowToList() {
    if(!!this._idRowToUpdate){
      this._tempRow['_id'] = this._idRowToUpdate;
      this.listService.updateRow(this.list.id, this._tempRow);
      this.resetFormRow();
    }else {
      if (Object.keys(this._tempRow).length > 0) {
        this.listService.addRowInList(this.list.id, this._tempRow);
        this.resetFormRow();
      }
    }
  }

  //load the row from list for edit
  public editRow(idRow: any) {
    this.listService
      .getRowById(idRow)
      .then(row => {
        this._idRowToUpdate = row['_id'];
        delete row['_id'];
        this._tempRow = row;
        Object.keys(this.utilMapRowProp)
          .forEach( key => {
            this.formRow.get(key).setValue(this._tempRow[key]);
          })
      });
  }

  //delete row from list by id
  public deleteRow(idRow: any) {
    this.listService.deleteRowFromList(this.list.id, idRow);
  }

  //useful mapping for user experience
  public getTranslatedPropriety(propriety: string){
    return this.utilMapRowProp[propriety];
  }

  //edit propriety-list when one of that is added
  public changePropriety(propriety: string) {
   this.rowPropriety = propriety;
  }

  //save propriety-list in tempRow (not in db)
  public setProprietyToRow() {
    this._tempRow[this.rowPropriety] = this.formRow.value[this.rowPropriety] != null ? this.formRow.value[this.rowPropriety] : '';
    //console.log(this._tempRow[this.rowPropriety]);
    //Avanzo alla proprietà successiva
    let keys = Object.keys(this.utilMapRowProp);
    let actualIndex = keys.indexOf(this.rowPropriety);
    if (actualIndex < (keys.length - 1)) {
      this.rowPropriety = keys[actualIndex + 1];
    }
  }

  //clear the form
  public resetFormRow() {
    let actualBox: number = this.formRow.get('case').value != null ? this.formRow.get('case').value : 0;
    this.formRow.reset({case: actualBox});
    this._idRowToUpdate = false;
    this._resetTempRow();
    this.rowPropriety = 'brand';
    this._tempRow.case = actualBox;
  }

  //increase/decrease to one the quantity of the row
  public editQuantity(idRow: string, operation: string){
    this.listService
      .getRowById(idRow)
      .then(row => {
        if(operation == 'add')
          row['quantity'] = row['quantity'] != null ? row['quantity'] + 1 : 1;
        else if(operation == 'remove')
          row['quantity'] = (row['quantity'] != null && row['quantity'] != 0) ? row['quantity'] - 1 : 0;
        this.listService.updateRow(this.list.id, row);
      });
  }

  public handleToggleProduct(p: IToggleProduct): void {
    this.formRow.get(p.element).setValue(p.valueSelected);
    this.setProprietyToRow();
  }

  //Handle numeric pad
  public handleToggleNumber(n: IToggleNumber) {
    let actualValue = this.formRow.get(n.element).value;
    let newNumber;

    //se il campo contiene già dei valori
    if(actualValue != null && actualValue != 0) {
      //se il comando è quello di canc
      switch (n.value) {
        case 999:
          newNumber = Number(actualValue.toString().slice(0, -1))
          break;
        case 99:
          //newNumber = actualValue + '' + n.value;
          newNumber = actualValue + '.';
          break;
        default:
          newNumber = actualValue + '' + n.value;
          newNumber = parseFloat(newNumber);
          break;

      }
    }else{
     if(n.value != 999 && n.value != 99)
       newNumber = n.value;
    }
    if(newNumber != undefined) {
      this.formRow.get(n.element).setValue(newNumber);
    }
  }
}
