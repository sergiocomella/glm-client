import {Component, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {FormControl, FormGroup} from '@angular/forms';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';
import {ListService} from '../../../services/list.service';
import {List} from '../../../models/list.model';

@Component({
  selector: 'app-lists-table',
  templateUrl: './lists-table.component.html',
  styleUrls: ['./lists-table.component.css']
})
export class ListsTableComponent implements OnInit {

  private _listSub: Subscription;
  form: FormGroup;
  displayedColumns: string[] = ['listTitle', 'updatedAt', 'options'];
  listService: ListService;
  lists: any;

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  isLoading: boolean = false;

  constructor(listService: ListService, private _router: Router) {
    this.listService = listService;
  }

  ngOnInit() {
    this.isLoading = true;
    this.listService.getAllLists();

    this._listSub = this.listService
      .getListsUpdateListener()
      .subscribe((listData: { lists: List[] }) => {
        this.isLoading = false;
        this.lists = new MatTableDataSource(listData.lists);
        this.lists.sort = this.sort;
      });
  }

  editList(id: any) {
    this._router.navigate(["/list-form", id], )
  }

  downloadList(id: string, listTitle: string) {
    this.listService.downloadList(id, listTitle);
  }

  deleteList(id: any, listTitle: string) {
    if(confirm('Stai per eliminare la lista: ' +  listTitle + '. \n Confermi? ')) {
      this.listService.deleteList(id);
    }
  }
}
