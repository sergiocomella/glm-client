import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IToggleNumber} from '../../../interfaces/IToggleNumber.interface';

@Component({
  selector: 'app-number-button',
  templateUrl: './number-button.component.html',
  styleUrls: ['./number-button.component.css']
})
export class NumberButtonComponent implements OnInit {

  numbers: number[];

  @Input()
  public element: any;

  @Output()
  public toggleProduct: EventEmitter<IToggleNumber> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    this.numbers = [1,2,3,4,5,6,7,8,9,99,0,999];
  }

  public handleNumberClick(element: string, value: number): void{
    /**
     * Propago un evento all'esterno di questo componente
     */
    this.toggleProduct.emit({
      element: element,
      value: value
    });

  }
}
