import {Component, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';
import {TypologyService} from '../../../services/typology.service';
import {Typology} from '../../../models/typology.model';

@Component({
  selector: 'app-typologies-list',
  templateUrl: './typologies-list.component.html',
  styleUrls: ['./typologies-list.component.css']
})
export class TypologiesListComponent implements OnInit {

  private _typologySub: Subscription;
  form: FormGroup;
  displayedColumns: string[] = ['typologyName', 'options'];
  typologiesService: TypologyService;
  typologies: any;

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  isLoading: boolean = false;

  constructor(typologiesService: TypologyService, private _router: Router) {
    this.typologiesService = typologiesService;
  }

  ngOnInit() {
    this.isLoading = true;
    this.typologiesService.getAllTypologies();

    this._typologySub = this.typologiesService
      .getTypologyUpdateListener()
      .subscribe((typologyData: {typologies: Typology[]}) => {
        this.isLoading = false;
        this.typologies = new MatTableDataSource(typologyData.typologies);
        this.typologies.sort = this.sort;
      });

    this.form = new FormGroup({
      typologyName: new FormControl(null)
    })
  }

  deleteTypology(id: string) {
    this.typologiesService.deleteTypology(id).subscribe(() =>
    {
      this.typologiesService.getAllTypologies();
    });
  }

  addTypology() {
    if (this.form.invalid) {
      return;
    }
    this.typologiesService
      .addTypology(this.form.value.typologyName);

    this.form.reset();
  }

}
