import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypologiesListComponent } from './typologies-list.component';

describe('TypologiesListComponent', () => {
  let component: TypologiesListComponent;
  let fixture: ComponentFixture<TypologiesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypologiesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypologiesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
