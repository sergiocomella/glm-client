import {Component, OnInit, ViewChild} from '@angular/core';
import {CategoriesService} from '../../../services/category.service';
import {Subscription} from 'rxjs';
import {Category} from '../../../models/category.model';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.css']
})
export class CategoriesListComponent implements OnInit {

  private _categorySub: Subscription;
  form: FormGroup;
  displayedColumns: string[] = ['categoryName', 'options'];
  categoriesService: CategoriesService;
  categories: any;

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  isLoading: boolean = false;

  constructor(categoriesService: CategoriesService, private _router: Router) {
    this.categoriesService = categoriesService;
  }

  ngOnInit() {
    this.isLoading = true;
    this.categoriesService.getAllCategories();

    this._categorySub = this.categoriesService
      .getCategoryUpdateListener()
      .subscribe((categoryData: {categories: Category[]}) => {
        this.isLoading = false;
        this.categories = new MatTableDataSource(categoryData.categories);
        this.categories.sort = this.sort;
      });

    this.form = new FormGroup({
      categoryName: new FormControl(null, {})
    })
  }

  deleteCategory(id: string) {
    this.categoriesService.deleteCategory(id).subscribe(() =>
    {
      this.categoriesService.getAllCategories();
    });
  }

  addCategory() {
    if (this.form.invalid) {
      return;
    }
    this.categoriesService
      .addCategory(this.form.value.categoryName);

    this.form.setValue({
      categoryName: ''
    })
  }


}
