import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {Router} from '@angular/router';
import {Brand} from '../../../models/brand.model';
import {BrandService} from '../../../services/brand.service';

@Component({
  selector: 'app-brands-list',
  templateUrl: './brands-list.component.html',
  styleUrls: ['./brands-list.component.css']
})
export class BrandsListComponent implements OnInit, OnDestroy {

  private _brandSub: Subscription;
  form: FormGroup;
  displayedColumns: string[] = ['brandName', 'options'];
  brandService: BrandService;
  brands: any;

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  isLoading: boolean = false;

  constructor(brandService: BrandService, private _router: Router) {
    this.brandService = brandService;
  }

  ngOnInit() {
    this.isLoading = true;
    this.brandService.getAllBrands();

    this._brandSub = this.brandService
      .getBrandUpdateListener()
      .subscribe((brandData: { brands: Brand[] }) => {
        this.isLoading = false;
        this.brands = new MatTableDataSource(brandData.brands);
        this.brands.sort = this.sort;
      });

    this.form = new FormGroup({
      brandName: new FormControl(null, {
        //validators: [Validators.required, Validators.minLength(3)]
      })
    })
  }

  deleteBrand(id: string) {
    this.brandService.deleteBrand(id).subscribe(() => {
      this.brandService.getAllBrands();
    });
  }

  addBrand() {
    if (this.form.invalid) {
      return;
    }
    this.brandService
      .addBrand(this.form.value.brandName);

    this.form
      .setValue({ brandName: '' })
  }

  ngOnDestroy(): void {
    this._brandSub.unsubscribe();
  }
}
