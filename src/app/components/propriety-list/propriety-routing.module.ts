import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import {BrandsListComponent} from './brands-list/brands-list.component';
import {AuthGuard} from '../auth/auth.guard';
import {CategoriesListComponent} from './categories-list/categories-list.component';
import {TypologiesListComponent} from './typologies-list/typologies-list.component';

const routers: Routes = [
  { path: "brands", component: BrandsListComponent, canActivate: [AuthGuard]},
  { path: "categories", component: CategoriesListComponent, canActivate: [AuthGuard]},
  { path: "typologies", component: TypologiesListComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routers)],
  exports: [RouterModule]
})
export class ProprietyRoutingModule {}
