import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AngularMaterialModule } from "../../angular-material.module";
import {BrandsListComponent} from './brands-list/brands-list.component';
import {CategoriesListComponent} from './categories-list/categories-list.component';
import {TypologiesListComponent} from './typologies-list/typologies-list.component';
import {ProprietyRoutingModule} from './propriety-routing.module';

@NgModule({
  declarations: [
    BrandsListComponent,
    CategoriesListComponent,
    TypologiesListComponent
  ],
  exports: [
    BrandsListComponent,
    CategoriesListComponent,
    TypologiesListComponent,
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ProprietyRoutingModule]
})
export class ProprietyModule {}
