import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public userIsAutheticated: boolean;
  private authListenerSubs: Subscription;

  constructor(public authService: AuthService) { }

  ngOnInit() {
    this.userIsAutheticated = this.authService.getIsAuth();
    this.authListenerSubs = this.authService.getAuthStatusListener().subscribe(isAuthenticated => {
      this.userIsAutheticated = isAuthenticated;
    });
  }

}
