import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {AuthGuard} from './components/auth/auth.guard';


const routes: Routes = [
  { path: "", component: HomeComponent, canActivate: [AuthGuard]},
  {
    path: "auth",
    loadChildren: () => import("./components/auth/auth.module").then(m => m.AuthModule)
  }, //Per rimappare le routes del child
  {
    path: "propriety",
    loadChildren: () => import("./components/propriety-list/propriety.module").then(m => m.ProprietyModule)
  },
  {
    path: "list",
    loadChildren: () => import("./components/list/list.module").then(m => m.ListModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
