import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'sortListByName' })
export class SortListByName implements PipeTransform {
  transform(array: any, field: string): any[] {
    if (!Array.isArray(array)) {
      return;
    }
   array.sort((a, b) => a[field].localeCompare(b[field]));
    return array;
  }
}
